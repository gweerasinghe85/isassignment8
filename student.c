#include<stdio.h>

struct student
{
	char name[50];
	char subject[50];
	int num;
	int marks;
};

int main()
{
	struct student stu[5];
	int i;
	printf("Enter information of students: \n");
	for (i=0; i<5; i++)
	{
		stu[i].num=i+1;
		printf("\nFor student no. %d,\n",stu[i].num);
		printf("\nEnter the name: ");
		scanf("%s",stu[i].name);	
		printf("Enter the subject: ");
		scanf("%s",stu[i].subject);	
		printf("Enter the marks: ");
		scanf("%d",&stu[i].marks);	
	}
	printf("\nDisplaying information.\n");
	for(i=0;i<5;i++)
	{
		printf("Student no. %d: name: %s \t Subject: %s \t Marks: %d \t \n",i+1,stu[i].name,stu[i].subject,stu[i].marks);
	}
	return 0;
}
